package com.example.geodemo2

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View

import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.Toast

import kotlinx.android.synthetic.main.activity_main.*
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.geodemo2.R
import kotlinx.android.synthetic.main.pager_item.*
import android.content.Intent


class ImagePagerAdapter(var context: Context, private var images: ArrayList<Int>, val countryCodes: Map<Int,Int>) : PagerAdapter() {

    override fun getCount(): Int {
        return images.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): View {

        //create image variable and inflater for the layout
        val image: ImageView
        val inflater: LayoutInflater =
            context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

        val itemView =
            inflater.inflate(
                R.layout.pager_item, container,
                false)


        // get reference to imageView in pager_item layout
        image = itemView.findViewById<View>(R.id.imageView) as ImageView

        // Set an image to the ImageView
        image.setImageResource(images[position])

        //This is the Int value which contains a reference to the path
        //of each image, currently what is on top of the arraylist
        var currentImg = images[position]

        //whenever the image is clicked
        image.setOnClickListener(){

            //If the current image is a value in our map (answer side of the card)
            if(countryCodes.containsValue(currentImg)){

                //filter the map so that it selects the key that cooresponds to the given value
                val filteredMap = countryCodes.filterValues { it == currentImg  }.keys
                currentImg = filteredMap.elementAt(0)

                //set the image reference to the key (flag side of the flashcard)
                image.setImageResource(currentImg)
            }

            // if the image is a key in our map (flag side of flashcard)
            else if(countryCodes.containsKey(currentImg)){

                //switch image to the value in our map (answer side of flashcard)
                currentImg = countryCodes[currentImg]!!
                image.setImageResource(currentImg)

            }
        }

        // Add pager_item layout as the current page to the ViewPager
        (container as ViewPager).addView(itemView)

        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {

        // Remove pager_item layout from ViewPager
        (container as ViewPager).removeView(`object` as RelativeLayout)
    }
}