// Adapted partially from Chapter 25 of Andoroid Programming in Kotlin For Beginners
//by John Horton

package com.example.geodemo2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.viewpager.widget.ViewPager
import androidx.viewpager.widget.PagerAdapter
import kotlinx.android.synthetic.main.pager_item.*
import java.util.ArrayList

class MainActivity : AppCompatActivity() {



    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        // Grab all the images and stuff them in our an arraylist
        val images = arrayListOf<Int>(
                R.drawable.ag,
                R.drawable.al,
                R.drawable.am,
                R.drawable.ao,
                R.drawable.ar,
                R.drawable.at,
                R.drawable.au,
                R.drawable.az,
                R.drawable.ba,
                R.drawable.bb,
                R.drawable.bd,
                R.drawable.be,
                R.drawable.bf,
                R.drawable.bg,
                R.drawable.bh,
                R.drawable.bi,
                R.drawable.bj,
                R.drawable.bn,
                R.drawable.bo,
                R.drawable.br,
                R.drawable.bs,
                R.drawable.bt,
                R.drawable.bw,
                R.drawable.by,
                R.drawable.bz,
                R.drawable.ca,
                R.drawable.cd,
                R.drawable.cf,
                R.drawable.cg,
                R.drawable.cg,
                R.drawable.ci,
                R.drawable.ck,
                R.drawable.cl,
                R.drawable.cm,
                R.drawable.cn,
                R.drawable.co,
                R.drawable.cr,
                R.drawable.cu,
                R.drawable.cv,
                R.drawable.cy,
                R.drawable.cz,
                R.drawable.de,
                R.drawable.dj,
                R.drawable.dk,
                R.drawable.dm,
                R.drawable.ec,
                R.drawable.ee,
                R.drawable.eg,
                R.drawable.eh,
                R.drawable.er,
                R.drawable.es,
                R.drawable.et,
                R.drawable.fi,
                R.drawable.fj,
                R.drawable.fm,
                R.drawable.fr,
                R.drawable.ga,
                R.drawable.gb,
                R.drawable.gd,
                R.drawable.ge,
                R.drawable.gh,
                R.drawable.gm,
                R.drawable.gn,
                R.drawable.gq,
                R.drawable.gr,
                R.drawable.gt,
                R.drawable.gw,
                R.drawable.gy,
                R.drawable.hn,
                R.drawable.hr,
                R.drawable.ht,
                R.drawable.hu,
                R.drawable.id,
                R.drawable.ie,
                R.drawable.il,
                R.drawable.iq,
                R.drawable.ir,
                R.drawable.it,
                R.drawable.jm,
                R.drawable.jo,
                R.drawable.jp,
                R.drawable.ke,
                R.drawable.kg,
                R.drawable.kh,
                R.drawable.ki,
                R.drawable.km,
                R.drawable.kn,
                R.drawable.kp,
                R.drawable.kr,
                R.drawable.kw,
                R.drawable.kz,
                R.drawable.la,
                R.drawable.lb,
                R.drawable.lc,
                R.drawable.li,
                R.drawable.lk,
                R.drawable.lr,
                R.drawable.ls,
                R.drawable.lt,
                R.drawable.lu,
                R.drawable.lv,
                R.drawable.ly,
                R.drawable.ma,
                R.drawable.mc,
                R.drawable.md,
                R.drawable.me,
                R.drawable.mg,
                R.drawable.mh,
                R.drawable.mk,
                R.drawable.ml,
                R.drawable.mm,
                R.drawable.mn,
                R.drawable.mr,
                R.drawable.mt,
                R.drawable.mu,
                R.drawable.mv,
                R.drawable.mw,
                R.drawable.mx,
                R.drawable.my,
                R.drawable.mz,
                R.drawable.na,
                R.drawable.ne,
                R.drawable.ng,
                R.drawable.ni,
                R.drawable.nl,
                R.drawable.no,
                R.drawable.np,
                R.drawable.nr,
                R.drawable.nu,
                R.drawable.nz,
                R.drawable.om,
                R.drawable.pa,
                R.drawable.pe,
                R.drawable.pg,
                R.drawable.ph,
                R.drawable.pk,
                R.drawable.pl,
                R.drawable.pt,
                R.drawable.pw,
                R.drawable.py,
                R.drawable.qa,
                R.drawable.ro,
                R.drawable.ru,
                R.drawable.rw,
                R.drawable.sa,
                R.drawable.sb,
                R.drawable.sc,
                R.drawable.sd,
                R.drawable.se,
                R.drawable.sg,
                R.drawable.si,
                R.drawable.sk,
                R.drawable.sl,
                R.drawable.sm,
                R.drawable.sn,
                R.drawable.so,
                R.drawable.sr,
                R.drawable.ss,
                R.drawable.st,
                R.drawable.sv,
                R.drawable.sy,
                R.drawable.sz,
                R.drawable.td,
                R.drawable.tg,
                R.drawable.th,
                R.drawable.tj,
                R.drawable.tl,
                R.drawable.tm,
                R.drawable.tn,
                R.drawable.to,
                R.drawable.tr,
                R.drawable.tt,
                R.drawable.tv,
                R.drawable.tw,
                R.drawable.tz,
                R.drawable.ua,
                R.drawable.ug,
                R.drawable.us,
                R.drawable.uy,
                R.drawable.uz,
                R.drawable.va,
                R.drawable.vc,
                R.drawable.ve,
                R.drawable.vn,
                R.drawable.vu,
                R.drawable.ws,
                R.drawable.xk,
                R.drawable.ye,
                R.drawable.za,
                R.drawable.zm,
                R.drawable.zw
            )

        // Map each image to another image of the correct answer to the flashcard,
        //these are the maps of each country
        //Flag images are from flagpedia.net
        //most map images are from worldmapsonline.com
        //some are taken from lonelyplanet.com
        val countryCodes = mapOf(
            R.drawable.ag to R.drawable.ag2,
            R.drawable.al to R.drawable.al2,
            R.drawable.am to R.drawable.am2,
            R.drawable.ao to R.drawable.ao2,
            R.drawable.ar to R.drawable.ar2,
            R.drawable.at to R.drawable.at2,
            R.drawable.au to R.drawable.au2,
            R.drawable.az to R.drawable.az2,
            R.drawable.ba to R.drawable.ba2,
            R.drawable.bb to R.drawable.bb2,
            R.drawable.bd to R.drawable.bd2,
            R.drawable.be to R.drawable.be2,
            R.drawable.bf to R.drawable.bf2,
            R.drawable.bg to R.drawable.bg2,
            R.drawable.bh to R.drawable.bh2,
            R.drawable.bi to R.drawable.bi2,
            R.drawable.bj to R.drawable.bj2,
            R.drawable.bn to R.drawable.bn2,
            R.drawable.bo to R.drawable.bo2,
            R.drawable.br to R.drawable.br2,
            R.drawable.bs to R.drawable.bs2,
            R.drawable.bt to R.drawable.bt2,
            R.drawable.bw to R.drawable.bw2,
            R.drawable.by to R.drawable.by2,
            R.drawable.bz to R.drawable.bz2,
            R.drawable.ca to R.drawable.ca2,
            R.drawable.cd to R.drawable.cd2,
            R.drawable.cf to R.drawable.cf2,
            R.drawable.cg to R.drawable.cg2,
            R.drawable.ch to R.drawable.ch2,
            R.drawable.ci to R.drawable.ci2,
            R.drawable.ck to R.drawable.ck2,
            R.drawable.cl to R.drawable.cl2,
            R.drawable.cm to R.drawable.cm2,
            R.drawable.cn to R.drawable.cn2,
            R.drawable.co to R.drawable.co2,
            R.drawable.cr to R.drawable.cr2,
            R.drawable.cu to R.drawable.cu2,
            R.drawable.cv to R.drawable.cv2,
            R.drawable.cy to R.drawable.cy2,
            R.drawable.cz to R.drawable.cz2,
            R.drawable.de to R.drawable.de2,
            R.drawable.dj to R.drawable.dj2,
            R.drawable.dk to R.drawable.dk2,
            R.drawable.dm to R.drawable.dm2,
            R.drawable.ec to R.drawable.ec2,
            R.drawable.ee to R.drawable.ee2,
            R.drawable.eg to R.drawable.eg2,
            R.drawable.eh to R.drawable.eh2,
            R.drawable.er to R.drawable.er2,
            R.drawable.es to R.drawable.es2,
            R.drawable.et to R.drawable.et2,
            R.drawable.fi to R.drawable.fi2,
            R.drawable.fj to R.drawable.fj2,
            R.drawable.fm to R.drawable.fm2,
            R.drawable.fr to R.drawable.fr2,
            R.drawable.ga to R.drawable.ga2,
            R.drawable.gb to R.drawable.gb2,
            R.drawable.gd to R.drawable.gd2,
            R.drawable.ge to R.drawable.ge2,
            R.drawable.gh to R.drawable.gh2,
            R.drawable.gm to R.drawable.gm2,
            R.drawable.gn to R.drawable.gn2,
            R.drawable.gq to R.drawable.gq2,
            R.drawable.gr to R.drawable.gr2,
            R.drawable.gt to R.drawable.gt2,
            R.drawable.gw to R.drawable.gw2,
            R.drawable.gy to R.drawable.gy2,
            R.drawable.hn to R.drawable.hn2,
            R.drawable.hr to R.drawable.hr2,
            R.drawable.ht to R.drawable.ht2,
            R.drawable.hu to R.drawable.hu2,
            R.drawable.id to R.drawable.id2,
            R.drawable.ie to R.drawable.ie2,
            R.drawable.il to R.drawable.il2,
            R.drawable.iq to R.drawable.iq2,
            R.drawable.ir to R.drawable.ir2,
            R.drawable.it to R.drawable.it2,
            R.drawable.jm to R.drawable.jm2,
            R.drawable.jo to R.drawable.jo2,
            R.drawable.jp to R.drawable.jp2,
            R.drawable.ke to R.drawable.ke2,
            R.drawable.kg to R.drawable.kg2,
            R.drawable.kh to R.drawable.kh2,
            R.drawable.ki to R.drawable.ki2,
            R.drawable.km to R.drawable.km2,
            R.drawable.kn to R.drawable.kn2,
            R.drawable.kp to R.drawable.kp2,
            R.drawable.kr to R.drawable.kr2,
            R.drawable.kw to R.drawable.kw2,
            R.drawable.kz to R.drawable.kz2,
            R.drawable.la to R.drawable.la2,
            R.drawable.lb to R.drawable.lb2,
            R.drawable.lc to R.drawable.lc2,
            R.drawable.li to R.drawable.li2,
            R.drawable.lk to R.drawable.lk2,
            R.drawable.lr to R.drawable.lr2,
            R.drawable.ls to R.drawable.ls2,
            R.drawable.lt to R.drawable.lt2,
            R.drawable.lu to R.drawable.lu2,
            R.drawable.lv to R.drawable.lv2,
            R.drawable.ly to R.drawable.ly2,
            R.drawable.ma to R.drawable.ma2,
            R.drawable.mc to R.drawable.mc2,
            R.drawable.md to R.drawable.md2,
            R.drawable.me to R.drawable.me2,
            R.drawable.mg to R.drawable.mg2,
            R.drawable.mh to R.drawable.mh2,
            R.drawable.mk to R.drawable.mk2,
            R.drawable.ml to R.drawable.ml2,
            R.drawable.mm to R.drawable.mm2,
            R.drawable.mn to R.drawable.mn2,
            R.drawable.mr to R.drawable.mr2,
            R.drawable.mt to R.drawable.mt2,
            R.drawable.mu to R.drawable.mu2,
            R.drawable.mv to R.drawable.mv2,
            R.drawable.mw to R.drawable.mw2,
            R.drawable.mx to R.drawable.mx2,
            R.drawable.my to R.drawable.my2,
            R.drawable.mz to R.drawable.mz2,
            R.drawable.na to R.drawable.na2,
            R.drawable.ne to R.drawable.ne2,
            R.drawable.ng to R.drawable.ng2,
            R.drawable.ni to R.drawable.ni2,
            R.drawable.nl to R.drawable.nl2,
            R.drawable.no to R.drawable.no2,
            R.drawable.np to R.drawable.np2,
            R.drawable.nr to R.drawable.nr2,
            R.drawable.nu to R.drawable.nu2,
            R.drawable.nz to R.drawable.nz2,
            R.drawable.om to R.drawable.om2,
            R.drawable.pa to R.drawable.pa2,
            R.drawable.pe to R.drawable.pe2,
            R.drawable.pg to R.drawable.pg2,
            R.drawable.ph to R.drawable.ph2,
            R.drawable.pk to R.drawable.pk2,
            R.drawable.pl to R.drawable.pl2,
            R.drawable.pt to R.drawable.pt2,
            R.drawable.pw to R.drawable.pw2,
            R.drawable.py to R.drawable.py2,
            R.drawable.qa to R.drawable.qa2,
            R.drawable.ro to R.drawable.ro2,
            R.drawable.ru to R.drawable.ru2,
            R.drawable.rw to R.drawable.rw2,
            R.drawable.sa to R.drawable.sa2,
            R.drawable.sb to R.drawable.sb2,
            R.drawable.sc to R.drawable.sc2,
            R.drawable.sd to R.drawable.sd2,
            R.drawable.se to R.drawable.se2,
            R.drawable.sg to R.drawable.sg2,
            R.drawable.si to R.drawable.si2,
            R.drawable.sk to R.drawable.sk2,
            R.drawable.sl to R.drawable.sl2,
            R.drawable.sm to R.drawable.sm2,
            R.drawable.sn to R.drawable.sn2,
            R.drawable.so to R.drawable.so2,
            R.drawable.sr to R.drawable.sr2,
            R.drawable.ss to R.drawable.ss2,
            R.drawable.st to R.drawable.st2,
            R.drawable.sv to R.drawable.sv2,
            R.drawable.sy to R.drawable.sy2,
            R.drawable.sz to R.drawable.sz2,
            R.drawable.td to R.drawable.td2,
            R.drawable.tg to R.drawable.tg2,
            R.drawable.th to R.drawable.th2,
            R.drawable.tj to R.drawable.tj2,
            R.drawable.tl to R.drawable.tl2,
            R.drawable.tm to R.drawable.tm2,
            R.drawable.tn to R.drawable.tn2,
            R.drawable.to to R.drawable.to2,
            R.drawable.tr to R.drawable.tr2,
            R.drawable.tt to R.drawable.tt2,
            R.drawable.tv to R.drawable.tv2,
            R.drawable.tw to R.drawable.tw2,
            R.drawable.tz to R.drawable.tz2,
            R.drawable.ua to R.drawable.ua2,
            R.drawable.ug to R.drawable.ug2,
            R.drawable.us to R.drawable.us2,
            R.drawable.uy to R.drawable.uy2,
            R.drawable.uz to R.drawable.uz2,
            R.drawable.va to R.drawable.va2,
            R.drawable.vc to R.drawable.vc2,
            R.drawable.ve to R.drawable.ve2,
            R.drawable.vn to R.drawable.vn2,
            R.drawable.vu to R.drawable.vu2,
            R.drawable.ws to R.drawable.ws2,
            R.drawable.xk to R.drawable.xk2,
            R.drawable.ye to R.drawable.ye2,
            R.drawable.za to R.drawable.za2,
            R.drawable.zm to R.drawable.zm2,
            R.drawable.zw to R.drawable.zw2
        )

        //shuffle the arraylist so the flashcards are in random order
        images.shuffle();

        // get a reference to the ViewPager in the layout
        val viewPager: ViewPager =
            findViewById<View>(R.id.pager) as ViewPager

        // Initialize the adapter
        val adapter: PagerAdapter =
            ImagePagerAdapter(this, images, countryCodes)

        // Binds the Adapter to the ViewPager
        viewPager.adapter = adapter

    }
}